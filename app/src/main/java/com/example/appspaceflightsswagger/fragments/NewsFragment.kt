package com.example.appspaceflightsswagger.fragments

import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.appspaceflightsswagger.R
import com.example.appspaceflightsswagger.adapters.RecyclerAdapter
import com.example.appspaceflightsswagger.base.BaseFragment
import com.example.appspaceflightsswagger.databinding.NewsFragmentBinding
import com.example.appspaceflightsswagger.viewModels.NewsViewModel

class NewsFragment : BaseFragment<NewsFragmentBinding, NewsViewModel>(
    NewsFragmentBinding::inflate,
    NewsViewModel::class.java
), SearchView.OnQueryTextListener {

    private lateinit var myAdapter: RecyclerAdapter

    override fun initialize(inflater: LayoutInflater, container: ViewGroup?) {
        init()
    }

    private fun init() {
        viewModel.init()
        initRecycler()
        observe()

    }

    private fun initRecycler() {
        myAdapter = RecyclerAdapter()
        binding.recyclerPhotos.adapter = myAdapter
    }

    private fun observe() {

        viewModel._countryLiveData.observe(viewLifecycleOwner, {
            myAdapter.setData(it.toMutableList())
        })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.main_menu, menu)

        val search = menu?.findItem(R.id.menu_search)
        val searchView = search.actionView as? SearchView
        searchView?.isSubmitButtonEnabled = true
        searchView?.setOnQueryTextListener(this)
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        return true
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        return true
    }
}