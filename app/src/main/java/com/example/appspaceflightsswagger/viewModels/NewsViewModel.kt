package com.example.appspaceflightsswagger.viewModels

import android.util.Log.d
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.appspaceflightsswagger.api.RetrofitInstance
import com.example.appspaceflightsswagger.models.NewsModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class NewsViewModel : ViewModel() {


    private val countryLiveData = MutableLiveData<List<NewsModel>>().apply {
        mutableListOf<NewsModel>()
    }

    val _countryLiveData: LiveData<List<NewsModel>> = countryLiveData


    fun init() {
        CoroutineScope(Dispatchers.IO).launch {
            getPhotos()
        }
    }


    private suspend fun getPhotos() {
        val result = RetrofitInstance.service.getNews()
        d("myLog", result.body().toString())

        if (result.isSuccessful) {
            val items = result.body()
            countryLiveData.postValue(items)
        }
    }

}