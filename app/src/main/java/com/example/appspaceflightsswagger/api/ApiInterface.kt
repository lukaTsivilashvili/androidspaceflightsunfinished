package com.example.appspaceflightsswagger.api

import com.example.appspaceflightsswagger.models.NewsModel
import retrofit2.Response
import retrofit2.http.GET

interface ApiInterface {
    @GET("v3/articles")
    suspend fun getNews(): Response<List<NewsModel>>
}