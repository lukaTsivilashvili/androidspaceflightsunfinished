package com.example.appspaceflightsswagger.adapters

import android.util.Log.d
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.appspaceflightsswagger.databinding.ItemPhotosBinding
import com.example.appspaceflightsswagger.extensions.loadImage
import com.example.appspaceflightsswagger.models.NewsModel

class RecyclerAdapter : RecyclerView.Adapter<RecyclerAdapter.MyViewholder>() {
    val infos: MutableList<NewsModel> = mutableListOf()

    inner class MyViewholder(private val binding: ItemPhotosBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private lateinit var model: NewsModel

        fun onBind() {
            var num = 0
            num++
            model = infos[adapterPosition]
            d("newLog", model.events.toString() + model.id)
            binding.imageCard.loadImage(model.imageUrl)
            binding.tvTitle.text = model.title
            binding.tvDate.text = model.updatedAt
            binding.tvDescription.text = model.summary
            if (model.events?.isEmpty() == true){
                binding.tvTimeline.text = "Empty"
            }else{
                binding.tvTimeline.text = model.events?.elementAt(adapterPosition)?.provider
            }


        }

    }

    fun setData(infos: MutableList<NewsModel>) {
        this.infos.addAll(infos)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewholder {
        val itemView =
            ItemPhotosBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        val holder = MyViewholder(itemView)
        return holder
    }

    override fun onBindViewHolder(holder: MyViewholder, position: Int) {
        holder.onBind()
    }

    override fun getItemCount() = infos.size

}