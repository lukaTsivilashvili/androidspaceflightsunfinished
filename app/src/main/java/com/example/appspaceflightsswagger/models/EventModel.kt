package com.example.appspaceflightsswagger.models

data class EventModel(

    val id:Int? = null,
    val provider:String? = null

)
