package com.example.appspaceflightsswagger.models

data class NewsModel(

    val id: Int,

    val title: String,

    val url: String,

    val imageUrl: String,

    val newsSite: String,

    val summary: String,

    val publishedAt: String,

    val updatedAt: String,

    val featured: Boolean,

    val events: List<EventModel>? = null,

    val launches:List<LaunchesModel>? = null

)
